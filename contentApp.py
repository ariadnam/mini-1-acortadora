#!/usr/bin/python3
import urllib

import webApp
import shelve
import random
from urllib.parse import parse_qs
import string

PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    {content}
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <p>Resource not found: {resource}.</p>
  </body>
</html>
"""

FORM = """<form action="/" method="POST" class="form-example">
  <div class="form-example">
    <label for="url">URL: </label>
    <input type="text" name="url" id="url" required />
  </div>
  <div class="form-example">
    <input type="submit" value="Submit" />
  </div>
</form>
"""

urls = {}

urls_acortardas = shelve.open('url_acort')
class contentApp(webApp.webApp):

    def __init__(self, hostname, port):
        self.contents = {}  # Es importante crearlo de este orden, creamos diccionario vacio
        super().__init__(hostname, port)

    def parse(self, request):
        data = {}
        body = {}
        parts = request.split(' ', 2)
        data['method'] = parts[0]
        data['resource'] = parts[1]

        body_start = request.find('\r\n\r\n')
        print(body_start)
        if body_start == -1:
            data['body'] = None
        else:
            if data['method'] == 'POST':
                body['url'] = request.split('\r\n\r\n')[1]
                data['body'] = body['url']
                print(data['body'])
            else:
                data['body'] = request[body_start:]
        return data

    def process(self, data):
        print(data)
        if data['method'] == 'GET':
            code, page = self.get(data['resource'])
        elif data['method'] == 'POST':
            code, page = self.post(data['resource'], data['body'])
        else:
            page = PAGE_NOT_FOUND.format(resource=data['resource'])
            code = "404 Resource Not Found"

        return code, page



    def get(self, resource):
        if resource == "/":
            code = "200 OK"
            page = "<p> Si quieres acortar una url, rellena el siguiente formulario: </p> " + FORM.format(
                resourceName=resource)
        elif resource in urls.keys():
            code = "301 Moved Permanently"

            page = "<p> <h1>You 're going to be redirected</h1> </p>" + '<p>Next page: <a href="' \
                   + urls[resource] + '">' + urls[resource] + "</a></p>"

        else:
            page = PAGE_NOT_FOUND.format(resource=resource)
            code = "404 Resource Not Found"

        return code, page

    def dict(self, dict_1):
        for k, v in dict_1.items():
            print(k, v)
    def key_random(self):
        caracter = string.ascii_letters + string.digits
        codigo_aleatorio = "/" + ''.join(random.choice(caracter) for _ in range(6))
        return codigo_aleatorio
    def post(self, resource, body):
        prefix = 'https://'
        print("estamossssssssss aquiii")
        print(urls)
        print(urls.values())
        parsed_body = parse_qs(body)
        if any(body in value for value in urls.values()):
            form_html = FORM.format(resourceName=resource)
            urls_list_html = "<p>Lista de URLs acortadas:</p><ul>"
            for key, url in urls.items():
                url = parsed_body['url'][0]
                urls[key] = parsed_body['url'][0]
                urls_list_html += f"<li><a href='{url}'>{key}</a></li>"
            page = PAGE.format(content=form_html + urls_list_html)
            code = "200 OK"
        elif parsed_body['url'][0].startswith('http://') or parsed_body['url'][0].startswith('https://'):
            urls[self.key_random()] = parsed_body['url'][0]
            form_html = FORM.format(resourceName=resource)
            urls_list_html = "<p>Lista de URLs acortadas:</p><ul>"
            for key, url in urls.items():
                urls_list_html += f"<li><a href='{url}'>{key}</a></li>"
            page = PAGE.format(content=form_html + urls_list_html)
            code = "200 OK"
        elif not parsed_body['url'][0].startswith('http://') or parsed_body['url'][0].startswith('https://'):
            url = prefix + parsed_body['url'][0]
            urls[self.key_random()] = url
            form_html = FORM.format(resourceName=resource)
            urls_list_html = "<p>Lista de URLs acortadas:</p><ul>"
            for key, url in urls.items():
                urls_list_html += f"<li><a href='{url}'>{key}</a></li>"
            page = PAGE.format(content=form_html + urls_list_html)
            code = "200 OK"
        else:
            page = PAGE_NOT_FOUND.format(resource=resource)
            code = "404 Resource Not Found"

        return code, page


if __name__ == "__main__":
    testWebApp = contentApp("localhost", 1234)